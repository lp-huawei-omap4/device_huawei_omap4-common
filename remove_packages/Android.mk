LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := RemovePackages
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := APPS
LOCAL_OVERRIDES_PACKAGES := \
    BasicDreams \
    CMBugReport \
    CMUpdater \
    Email \
    Exchange \
    Exchange2 \
    Galaxy4 \
    HoloSpiralWallpaper \
    LiveWallpapers \
    LiveWallpapersPicker \
    MagicSmokeWallpapers \
    NoiseField \
    PhaseBeam \
    PhotoPhase \
    PhotoTable \
    ResurrectionOTA \
    ResurrectionStats \
    UnifiedEmail \
    vim \
    vimrc \
    VisualizationWallpapers

LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED

include $(BUILD_PREBUILT)
